#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from bottle import Bottle, route, run, template, get, post, debug, static_file, request, redirect, response
from math import sin, cos, radians
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import datetime
import json
import time
import random
import string

import scrambler as scr
import random_scramble as rs


secretKey = "SDMDSIUDSFYODS&TTFS987f9ds7f8sd6DFOUFYWE&FY"

def zeroLeading(n):
    return (n > 9) * ("" + str(n)) + (not n > 9) *("0" + str(n))

def from_hd_to_time(hd):
    if(hd > 0): return str(time.strftime("%M:%S", time.gmtime(int(hd)/100))) + "." + zeroLeading(int(hd) % 100)
    else: return "---"

def get_hund_series(dataFrame):
    time_series = dataFrame.time.apply(lambda x: datetime.datetime.strptime(x, '%M:%S.%f')).reset_index(drop = True)
    time_series = time_series.apply(lambda x: x.hour*360000+x.minute*6000+x.second*100+int(x.microsecond/10000))
    return time_series

def get_statistics(hd, section):
    if section == "session":
        if(hd.size < 2): mean = best = std = 0
        else:
            mean = hd.mean()
            best = hd.min()
            std = 0
        if hd.size >= 2:
            std = hd.std()/100
        if(hd.size >= 3):
            rollingmean3 = hd.rolling(3).mean().iloc[::-1].reset_index(drop = True)
            mean_of_3 = rollingmean3[0]
            best_mean_of_3 = rollingmean3.min()
        else: mean_of_3 = best_mean_of_3 = 0
        if(hd.size >= 5):
            rollingmin5 = hd.rolling(5, min_periods = 1).min()
            rollingmax5 = hd.rolling(5, min_periods = 1).max()
            rollingsum5 = hd.rolling(5).sum()
            rollingavg5 = (rollingsum5 - rollingmax5 - rollingmin5).iloc[::-1].reset_index(drop = True).apply(lambda x: x/3)
            avg_of_5 = rollingavg5[0]
            best_avg_of_5 = rollingavg5.min()
        else: avg_of_5 = best_avg_of_5 = 0

        if(hd.size >= 12):
            rollingmin12 = hd.rolling(12, min_periods = 1).min()
            rollingmax12 = hd.rolling(12, min_periods = 1).max()
            rollingsum12 = hd.rolling(12).sum()
            rollingavg12 = (rollingsum12 - rollingmax12 - rollingmin12).iloc[::-1].reset_index(drop = True).apply(lambda x: x/10)
            avg_of_12 = rollingavg12[0]
            best_avg_of_12 = rollingavg12.min()
        else: avg_of_12 = best_avg_of_12 = 0

        hd_times = (mean, best_mean_of_3, best_avg_of_5, best_avg_of_12, mean_of_3, avg_of_5, avg_of_12, best)
        statistics = [from_hd_to_time(x) for x in hd_times]
        statistics.append(round(std, 2))

    elif section == "history":
        if(hd.size < 2): mean = best = std = quantile1 = quantile3 = 0
        else:
            mean = hd.mean()
            best = hd.min()
            quantiles = np.percentile(hd, [25, 75])
            quantile1 = quantiles[0]
            quantile3 = quantiles[1]
            std = 0
        if hd.size >= 2:
            std = hd.std()/100
        if(hd.size >= 3):
            rollingmean3 = hd.rolling(3).mean()
            best_mean_of_3 = rollingmean3.min()
        else: best_mean_of_3 = 0
        if(hd.size >= 5):
            rollingmin5 = hd.rolling(5, min_periods = 1).min()
            rollingmax5 = hd.rolling(5, min_periods = 1).max()
            rollingsum5 = hd.rolling(5).sum()
            rollingavg5 = (rollingsum5 - rollingmax5 - rollingmin5).iloc[::-1].apply(lambda x: x/3)
            best_avg_of_5 = rollingavg5.min()
        else: best_avg_of_5 = 0

        if(hd.size >= 12):
            rollingmin12 = hd.rolling(12, min_periods = 1).min()
            rollingmax12 = hd.rolling(12, min_periods = 1).max()
            rollingsum12 = hd.rolling(12).sum()
            rollingavg12 = (rollingsum12 - rollingmax12 - rollingmin12).iloc[::-1].apply(lambda x: x/10)
            best_avg_of_12 = rollingavg12.min()
        else: best_avg_of_12 = 0

        hd_times = (mean, best_mean_of_3, best_avg_of_5, best_avg_of_12, best, round(quantile1, 2), round(quantile3, 2))
        statistics = [from_hd_to_time(x) for x in hd_times]
        statistics += [round(std, 2)]

    else: raise Exception("Błąd w ustalaniu statystyk. Nie ma takiej sekcji: " + section)

    return statistics
    

def get_session_and_history():
    session_df = pd.read_pickle("session.p")
    cube_session_list = session_df.values.tolist()
    history_df = pd.read_pickle("history.p")
    cube_history_list = history_df.values.tolist()
    return (session_df, cube_session_list, history_df, cube_history_list)

app = Bottle()

@app.route('/generate_histogram', method = 'POST')
def generate_histogram():
    n = 20
    cube_type = int(request.get_cookie('cube_type', secret = secretKey))
    history_df = pd.read_pickle("history.p").query("cube_type == @cube_type")
    hd = get_hund_series(history_df)

    times_list = list(hd.sort_values())
    if hd.size > 0: tick = (hd.max()+50)/n
    else: tick = 50
    majorLocator = MultipleLocator(tick)
    # Tworzenie płótna, wykresu oraz określenie czasów i zakresu osi x
    fig = plt.figure(figsize=(15,10))
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xticks(times_list, minor = True)
    ax.set_xlim(0, tick*n)
    # Ustawienie podziału na osi x co wartość tick
    ax.xaxis.set_major_locator(majorLocator)
    # Ustawienie etykiet na podziałach
    ticks = ax.get_xticks().astype(float)
    ticklabels = map(from_hd_to_time, ticks)
    ax.set_xticklabels(ticklabels, rotation = 45)
    ax.tick_params(axis = 'both', which = 'major', length = 10, labelsize = 15)
    # Zapisanie wykresu
    ax.hist(hd, bins = ticks)
    fig.savefig("static/histogram" + str(cube_type) + ".png")
    return {}

@app.route('/export_to_excel', method = 'POST')
def export_to_excel():
    history_df = pd.read_pickle("history.p")
    two = history_df.query("cube_type == 2").reset_index(drop = True)
    three = history_df.query("cube_type == 3").reset_index(drop = True)
    four = history_df.query("cube_type == 4").reset_index(drop = True)

    two.index = two.index + 1
    three.index = three.index + 1
    four.index = four.index + 1
    history_df.index = history_df.index + 1

    writer = pd.ExcelWriter('Historia ułożeń.xlsx', engine='xlsxwriter')
    two.to_excel(writer, sheet_name='2x2x2')
    three.to_excel(writer, sheet_name='3x3x3')
    four.to_excel(writer, sheet_name='4x4x4')
    history_df.to_excel(writer, sheet_name='all')
    writer.save()

@app.route('/set_default', method = 'POST')
def set_default():
    
    cube_type = int(request.get_cookie('cube_type', secret = secretKey))
    session_df = pd.read_pickle("session.p").query("cube_type == @cube_type")
    history_df = pd.read_pickle("history.p").query("cube_type == @cube_type")
    session_list = session_df.values.tolist()
    history_list = history_df.values.tolist()

    session_hd = get_hund_series(session_df)
    history_hd = get_hund_series(history_df)

    session_statistics = get_statistics(session_hd, "session")
    history_statistics = get_statistics(history_hd, "history")

    return {"cube_type" : cube_type, "session_list" : session_list, "history_list" : history_list, "session_statistics" : session_statistics, "history_statistics" : history_statistics}

@app.route('/change_type', method = 'POST')
def change_type():
    new_cube_type = int(request.POST.get('new_cube_type'))
    response.set_cookie('cube_type', new_cube_type, secret = secretKey)

    session_df = pd.read_pickle("session.p").query("cube_type == @new_cube_type")
    history_df = pd.read_pickle("history.p").query("cube_type == @new_cube_type")
    session_list = session_df.values.tolist()
    history_list = history_df.values.tolist()

    session_hd = get_hund_series(session_df)
    history_hd = get_hund_series(history_df)
    session_statistics = get_statistics(session_hd, "session")
    history_statistics = get_statistics(history_hd, "history")
    return {"new_cube_type" : new_cube_type, "history_list" : history_list, "session_list" : session_list, "session_statistics" : session_statistics, "history_statistics" : history_statistics}

@app.route('/random_scramble', method = 'POST')
def random_scramble():
    cube_type = request.get_cookie('cube_type', secret = secretKey)
    return rs.random_scramble(cube_type)

@app.route('/animate_cube', method = 'POST')
def animate_cube():
    cube_type = request.get_cookie('cube_type', secret = secretKey)
    scramble = json.loads(request.POST.get('scramble'));
    rotations = scr.set_rotations(scramble, cube_type)
    return {"rotations" : rotations}

@app.route('/add_time', method = 'POST')
def add_time():
    date = request.POST.get("date")
    scramble = request.POST.get("scramble")
    time = request.POST.get("time")
    cube_type = request.get_cookie('cube_type', secret = secretKey)

    session_df = pd.read_pickle("session.p")
    history_df = pd.read_pickle("history.p")
    row = [date, scramble, time, cube_type]
    session_df = session_df.append(pd.Series(row, index = ["date", "scramble", "time", "cube_type"]), ignore_index = True)
    history_df = history_df.append(pd.Series(row, index = ["date", "scramble", "time", "cube_type"]), ignore_index = True)
    pd.to_pickle(session_df, path = "session.p")
    pd.to_pickle(history_df, path = "history.p")
    session_list = session_df.query("cube_type == @cube_type").values.tolist()
    history_list = history_df.query("cube_type == @cube_type").values.tolist()

    session_hd = get_hund_series(session_df.query("cube_type == @cube_type"))
    history_hd = get_hund_series(history_df.query("cube_type == @cube_type"))
    session_statistics = get_statistics(session_hd, "session")
    history_statistics = get_statistics(history_hd, "history")

    return {"cube_type" : cube_type, "history_list" : history_list, "session_list" : session_list, "session_statistics" : session_statistics, "history_statistics" : history_statistics}

@app.route('/static/:path#.+#', name='static')
def static(path):
    return static_file(path, root='./static')

@app.route('/webplayer/:path#.+#', name='webplayer')
def webplayer(path):
    return static_file(path, root='./webplayer')

@app.route('/')
@app.route('/index')
@app.route('/index/')
@app.route('/index/<message>')
def index(message=''):
    if('cube_type' not in request.cookies):
        response.set_cookie('cube_type', 3, secret = secretKey)
        pd.to_pickle(pd.DataFrame(columns=["date", "scramble", "time", "cube_type"]), path = "session.p")
    return template('index', randomScramble = " ", cube_type = request.get_cookie('cube_type', secret = secretKey))

app.run(host='localhost', port=63700, reloader=True, debug=False)
