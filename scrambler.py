from pyquaternion import Quaternion
from math import radians

moves = {}

wangle_val= {"Uw" : -90, "Uw'" : 90, "Dw" : -90, "Dw'" : 90, "Lw" : -90, "Lw'" : 90, "Rw" : -90, "Rw'" : 90, "Fw" : -90, "Fw'" : 90, "Bw" : -90, "Bw'" : 90, "Uw2" : 180, "Bw2" : 180, "Lw2" : 180, "Rw2" : 180, "Fw2" : 180, "Dw2" : 180}
waxes = {"Uw" : [0, 0, 1], "Uw'" : [0, 0, 1], "Dw" : [0, 0, -1], "Dw'" : [0, 0, -1], "Lw" : [0, -1, 0], "Lw'" : [0, -1, 0], "Rw" : [0, 1, 0], "Rw'" : [0, 1, 0], "Fw" : [1, 0, 0], "Fw'" : [1, 0, 0], "Bw" : [-1, 0, 0], "Bw'" : [-1, 0, 0], "Uw2" : [0, 0, 1], "Bw2" : [-1, 0, 0], "Lw2" : [0, -1, 0], "Rw2" : [0, 1, 0], "Fw2" : [1, 0, 0], "Dw2" : [0, 0, -1]}

angle_val = {"U" : -90, "U'" : 90, "D" : -90, "D'" : 90, "L" : -90, "L'" : 90, "R" : -90, "R'" : 90, "F" : -90, "F'" : 90, "B" : -90, "B'" : 90, "U2" : 180, "B2" : 180, "L2" : 180, "R2" : 180, "F2" : 180, "D2" : 180}
axes = {"U" : [0, 0, 1], "U'" : [0, 0, 1], "D" : [0, 0, -1], "D'" : [0, 0, -1], "L" : [0, -1, 0], "L'" : [0, -1, 0], "R" : [0, 1, 0], "R'" : [0, 1, 0], "F" : [1, 0, 0], "F'" : [1, 0, 0], "B" : [-1, 0, 0], "B'" : [-1, 0, 0], "U2" : [0, 0, 1], "B2" : [-1, 0, 0], "L2" : [0, -1, 0], "R2" : [0, 1, 0], "F2" : [1, 0, 0], "D2" : [0, 0, -1]}

angle_val.update(wangle_val)
axes.update(waxes)

def select(move, loc):
    selected = []
    elements = loc.keys()
    direction = moves[move][0]
    distance = moves[move][1]
    if(isinstance(distance, list)):
        for d in distance:
            for el in [el for el in elements if loc[el][direction] == d]:
                selected.append(el)
    else:
        for el in [el for el in elements if loc[el][direction] == distance]:
            selected.append(el)
    return selected

def set_moves(cube_type):
    if cube_type == 4:
        wmoves = {"Uw": (2, [1,2], 90), "Uw'": (2, [1,2], -90), "Rw": (1, [1,2], 90), "Rw'": (1, [1,2], -90), "Lw": (1, [-1,-2], 90), "Lw'": (1, [-1,-2], -90), "Fw": (0, [1,2], 90), "Fw'": (0, [1,2], -90), "Bw": (0, [-1,-2], 90), "Bw'": (0, [-1,-2], -90), "Dw": (2, [-1,-2], 90), "Dw'": (2, [-1,-2], -90), "Uw2": (2, [1,2], 180), "Dw2": (2, [-1,-2], 180), "Fw2": (0, [1,2], 180), "Bw2": (0, [-1,-2], 180), "Rw2": (1, [1,2], 180), "Lw2": (1, [-1,-2], 180)}
        moves = {"U": (2, 2, 90), "U'": (2, 2, -90), "R": (1, 2, 90), "R'": (1, 2, -90), "L": (1, -2, 90), "L'": (1, -2, -90), "F": (0, 2, 90), "F'": (0, 2, -90), "B": (0, -2, 90), "B'": (0, -2, -90), "D": (2, -2, 90), "D'": (2, -2, -90), "U2": (2, 2, 180), "D2": (2, -2, 180), "F2": (0, 2, 180), "B2": (0, -2, 180), "R2": (1, 2, 180), "L2": (1, -2, 180)}
        moves.update(wmoves)
        return moves
    elif cube_type in [2,3]:
        moves = {"U": (2, 1, 90), "U'": (2, 1, -90), "R": (1, 1, 90), "R'": (1, 1, -90), "L": (1, -1, 90), "L'": (1, -1, -90), "F": (0, 1, 90), "F'": (0, 1, -90), "B": (0, -1, 90), "B'": (0, -1, -90), "D": (2, -1, 90), "D'": (2, -1, -90), "U2": (2, 1, 180), "D2": (2, -1, 180), "F2": (0, 1, 180), "B2": (0, -1, 180), "R2": (1, 1, 180), "L2": (1, -1, 180)}
        return moves

def set_loc(cube_type, elements):
    loc = {}
    if cube_type == 4:
        for el in elements:
            loc[el] = [0, 0, 0]
            if (el.count("X")>0): loc[el][0] = -2
            if (el.count("Y")>0): loc[el][0] = -1
            if (el.count("Z")>0): loc[el][0] = 1
            if (el.count("W")>0): loc[el][0] = 2

            if (el.count("A")>0): loc[el][1] = -2
            if (el.count("B")>0): loc[el][1] = -1
            if (el.count("C")>0): loc[el][1] = 1
            if (el.count("D")>0): loc[el][1] = 2

            if (el.count("1")>0): loc[el][2] = -2
            if (el.count("2")>0): loc[el][2] = -1
            if (el.count("3")>0): loc[el][2] = 1
            if (el.count("4")>0): loc[el][2] = 2

    elif cube_type in [2,3]:
        for el in elements:
            loc[el] = [0, 0, 0]
            if (el.count("G")>0): loc[el][0] = 1
            if (el.count("B")>0): loc[el][0] = -1
            if (el.count("R")>0): loc[el][1] = 1
            if (el.count("O")>0): loc[el][1] = -1
            if (el.count("W")>0): loc[el][2] = 1
            if (el.count("Y")>0): loc[el][2] = -1

    return loc

def set_elements(cube_type):
    if cube_type == 4:
        return ['DZ1', 'DY1', 'X1D', 'W1C', 'C1Z', 'X1C', 'W1B', 'BZ1', 'BY1', 'W1A', 'A1Y', 'X1A', 'W2D', 'DZ2', 'DY2', 'X2D', 'W2C', 'X2C', 'W2B', 'X2B', 'Z2A', 'Y2A', 'X2A', 'W3D', 'DZ3', 'DY3', 'X3D', 'W3C', 'X3C', 'W3B', 'X3B', 'W3A', 'Z3A', 'Y3A', 'X3A', 'W4D', 'Z4D', 'X4D', 'W4C', 'Z4C', 'Y4C', 'WB4', 'Z4B', 'Y4B', 'X4B', 'WA4', 'Z4A', 'W1D', 'C1Y', 'X1B', 'A1Z', 'X4A', 'W2A', 'Y4D', 'X4C', 'Y4A']
    elif cube_type == 3:
        return ["BBB", "BOE", "BOW", "BOY", "BRE", "BRW", "BRY", "BWE", "BYE", "GGG", "GOE", "GOW", "GOY", "GRE", "GRW", "GRY", "GWE", "GYE", "III", "OOO", "OWE", "OYE", "RRR", "RWE", "RYE", "WWW", "YYY"]
    elif cube_type == 2:
        return ['GRY', 'BOW', 'BOY', 'GOY', 'BRY', 'BRW', 'GOW', 'GRW']
    else:
        raise Exception("Zły typ kostki. Błąd ustalania elementów")

def set_rotations(scramble, cube_type = 3):
    global moves
    global angle_val
    global axes
    elements = set_elements(cube_type)
    loc = set_loc(cube_type, elements)
    moves = set_moves(cube_type)
    rotations = {el : Quaternion([1, 0, 0, 0]) for el in elements}

    for move in scramble:
        quat = Quaternion(axis = axes[move], angle = radians(angle_val[move]))
        move_els = select(move, loc)
        for el in move_els:
            loc[el] = quat.rotate(loc[el])
            loc[el] = [int(round(el)) for el in loc[el]]
            rotations[el] = quat * rotations[el]
    rotations = {el : [q[1], q[2], q[3], q[0]] for el, q in rotations.items()}
    return rotations