# Opis ogólny:

Aplikacja webowa do mierzenia czasów ułożenia kostki Rubika i pochodnych (2x2x2, 4x4x4). Rozbudowana o statystyki oraz interaktywny model 3D.

# Szczegóły:

Aplikacja służy do mierzenia czasów ułożeń (przeznaczona dla speedcuberów). Automatycznie generuje scramble (notacja do mieszania kostki), wizualizuje układ kolorów na każdym etapie scramble'a oraz zapisuje zmierzone czasy wraz z datą itp w sesji oraz historii ułożeń. Na podstawie zebranych danych liczy statystyki takie jak:
- najlepsze ułożenie,
- aktualne średnie z 3, 5, 12 ułożeń*
- najlepsze średnie z 3, 5, 12 ułożeń oraz z całej sesji i historii*
- kwartyl 1 i 3, odchylenie standardowe

Aplikacja pozwala ustalić przycisk na klawiaturze do uruchamiania stopera oraz czas preinspekcji (czas nie liczony do ułożenia, przeznaczony na obejrzenie kostki). Po każdym zapisanym czasie uaktualnia histogram pokazujący rozłożenie czasów.

Aplikacja obsługuje kostki 2x2x2, 3x3x3, 4x4x4 - dla każdej generuje oddzielne statystyki oraz histogramy i pozwala na zmianę trybu w każdej chwili.
Posiada również przycisk, który eksportuje dane z historii do pliku xlsx z podziałem na 2x2x2, 3x3x3, 4x4x4 oraz all.

# Wymagane biblioteki:


    math
    bottle
    numpy
    pandas
    matplotlib
    datetime
    json
    time
    random
    string
    pyquaternion
    xlsxwriter


* - wg schematu obowiązującego w speedcubingu tzn dla średnich z 5 i 12 odrzuca się najlepszy i najsłabszy wynik i z reszty liczy się średnią.
