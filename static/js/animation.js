b4w.register("example_main", function(exports, require) {

var m_anim     = require("animation");
var m_app      = require("app");
var m_cfg      = require("config");
var m_cont     = require("container");
var m_data     = require("data");
var m_scs      = require("scenes");
var m_sfx      = require("sfx");
var m_version  = require("version");
var m_trans    = require("transform");

var timer;
var inspection;
var DEBUG = (m_version.type() === "DEBUG");

exports.init = function() {
    m_app.init({
        canvas_container_id: "canvas_cont",
        callback: init_cb,
        physics_enabled: false,
        alpha: true,
        assets_dds_available: !DEBUG,
        assets_min50_available: !DEBUG,
        report_init_failure: false,
        media_auto_activation: false
    });
}

function init_cb(canvas_elem, success) {
    lock(["a.cube-type", "div#random-scramble", "div#timer-container"]);
    files = {2 : "2.json", 3 : "3.json", 4 : "4.json"}
    if (!success) {
        console.log("b4w init failure");
        return;
    }

    function changed(){
        unlock(["a.cube-type", "div#random-scramble", "div#timer-container"]);
        $("div#scramble").fadeTo(0, 0, randomScramble);
        generateHistogram(cube_type); 
    }

            
    $.ajax({
        url: "/set_default",
        type: "post",
        dataType: "json",
        success: function(output){
            cube_type = output["cube_type"];
            session_list = output["session_list"];
            history_list = output["history_list"];
            session_statistics = output['session_statistics'];
            history_statistics = output['history_statistics'];
            fill_section("session", session_list);
            fill_section("history", history_list);
            fill_statistics("session", session_statistics);
            fill_statistics("history", history_statistics);
            load_path = "/webplayer/" + files[cube_type];
            m_data.load(load_path, changed);
            resize();
        }
    });
    window.addEventListener("resize", resize);
}

function resize() {
    m_cont.resize_to_container();
}

function change_type(new_cube_type){
    lock(["div#random-scramble", "div#timer-container"]);
    var forbidden = $("a.cube-type").hasClass("locked");

    if(!forbidden){
        lock(["a.cube-type"]);
        $("div#session-times").html("");
        var clicked_type = parseInt($(new_cube_type).attr('id'));
        files = {2 : "2.json", 3 : "3.json", 4 : "4.json"}
        load_path = "/webplayer/" + files[clicked_type];

        function changed(){
            unlock(["a.cube-type", "div#random-scramble", "div#timer-container"]);
            generateHistogram(clicked_type);
            $("div#scramble").fadeTo(0, 0, randomScramble);
        }

        $.ajax({
            url: "/change_type",
            type: "post",
            dataType: "json",
            data: {"new_cube_type" : clicked_type},
            success: function(output){
                session_list = output["session_list"];
                history_list = output["history_list"];
                session_statistics = output['session_statistics'];
                history_statistics = output['history_statistics'];
                m_data.unload();
                fill_statistics("session", session_statistics);
                fill_statistics("history", history_statistics);
                fill_section("session", session_list);
                fill_section("history", history_list);
                m_data.load(load_path, changed);
                resize();
            }
        });
    }
}

function animate_cube(){
    var scr = []
    $(".move").each(function(){
        scr.push($(this).text())
        if($(this).attr("id") == "last") return false;
    });
    $.ajax({
        url: "/animate_cube",
        type: "POST",
        data: {"scramble" : JSON.stringify(scr)},
        dataType: "json",
        success: function(output){
            var rotations = output["rotations"];
            $.each(rotations, function(el, q){
                var obj = m_scs.get_object_by_name(el);
                m_trans.set_rotation_v(obj, q);
            });
        }
    });
    m_app.enable_camera_controls();
}
function zeroLeading(n){
    return n > 9 ? "" + n: "0" + n;
}
function randomScramble(type){
    var forbidden = $("div#random-scramble").hasClass("locked");
    if(!forbidden){
        $.ajax({
            url: '/random_scramble',
            type: 'post',
            success: function(output){
                $("#scramble").html(output).fadeTo("fast", 1);
                $("div.move:last-child").attr("id", "last");
                animate_cube();
            }
        });
    }
}

function animate_sequence(last){
    $(".move").each(function(){
            $(this).removeAttr("id");
        });
    $(last).attr("id", "last");
    animate_cube();
}

function stopwatch_start(start){
    var end = new Date();
    var time = end-start;
    var milisec = zeroLeading(Math.floor((time%1000)/10));
    var sec = zeroLeading(Math.floor(time/1000)%60);
    var min = zeroLeading(Math.floor(time/60000));
    var formattedTime = [min, sec].join(":") + "." + milisec;
    $("div#timer").html(formattedTime);
}

function stopwatch_inspection(start){
    var end = new Date();
    var time = end-start;
    var milisec = zeroLeading(Math.floor((time%1000)/10));
    var sec = zeroLeading(Math.floor(time/1000)%60);
    $("div#timer").html(15-sec);
    if(time > 10*1000 && time < 10.5*1000){
        $("div#timer-container").addClass("last-seconds");
    }
    if(15*1000-time < 0){
        clearInterval(inspection);
        $("div#timer-container").removeClass("inspection last-seconds");
        unlock(["a.cube-type", "div#random-scramble", "div#timer-container"]);
        randomScramble();
        $("div#timer").html("00:00.00");
    }
}

function default_operations(){
    $("div.move:last-child").attr("id", "last");
    $("div#section-content > div").hide();
    $("#section-content > div.times").show();
    $("div#section-headers > div > div.times").attr("id", "active");
    $(document).mouseup(function(){
        $("div#timer-container").removeClass("button-down");
    });
    window.addEventListener('keydown', function(e) {
      if(e.which == 32 && e.target == document.body) {
        e.preventDefault();
      }
    });
    $(document).keyup(function(e) {
      if(e.which == 32) {
        e.preventDefault();
      }
    });
    $("#start").keydown(function(e) {
      if(e.keyCode == 10 || e.keyCode == 13 || e.keyCode == 32) {
        e.preventDefault();
      }
    });
}

function lock(list_of_elements){
    $.each(list_of_elements, function(id, el){
        $(el).addClass("locked");
    });
}

function unlock(list_of_elements){
    $.each(list_of_elements, function(id, el){
        $(el).removeClass("locked");
    });
}

function fill_section(section, list){
    $("div#section-content div#" + section + "-times").html("<table id=\"" + section + "-table\"></table>");
    var i = 1;
    $.each(list, function(id, row){
        var date = row[0];
        var scramble = row[1];
        var time = row[2];
        $("table#" + section + "-table").prepend("<tr><td class=\"id\">" + i + ".</td><td class=\"date\">" + date + "</td><td class=\"scramble\">" + scramble + "</td><td class=\"solve-time\">" + time + "</td></tr>");
        i++;
    });
}

function fill_statistics(section, list){
    $("div#" + section + "-data span").each(function(i){
        $(this).html(list[i]);
    });
}

function generateHistogram(cube_type){
    $.ajax({
        url: "/generate_histogram",
        dataType: 'json',
        type: 'post',
        success: function(output){
            $("div#histogram-image img").attr("src", "static/histogram" + cube_type + ".png?timestamp=" + new Date().getTime());
        }
    });
}
function formatDate(date){
    return [date.getFullYear(), date.getMonth()+1, date.getDate()].map(zeroLeading).join("-") + ", " + [date.getHours(), date.getMinutes(), date.getSeconds()].map(zeroLeading).join(":");
}

function properKey(e){
    if(e == true) return true;
    var selected_key = $("select#start").val();
    switch(selected_key){
        case "0":
            selected_key = 96;
            break;
        case "Space":
            selected_key = 32;
            break;
        case "Enter":
            selected_key = 13;
            break;
    }
    fit = (selected_key == e.which);
    return fit;
}
function timerDown(e){
    var e = e || true;
    var forbidden = $("div#timer-container").hasClass("locked");
    if(!forbidden && properKey(e)){
        $("div#timer-container").addClass("button-down");
    }
}
function timerUp(e){
    var e = e || true;
    var forbidden = $("div#timer-container").hasClass("locked");
    if(!forbidden && properKey(e)){
        $("div#timer-container").removeClass("button-down");
        if($("div#timer-container").hasClass("measure")){
            var time = $("div#timer").text();
            var scramble = [];
            $.each($("div.move"), function(){
                scramble.push($(this).text());
            })
            scramble = scramble.join(" ");
            date = formatDate(new Date());
            $.ajax({
                url: "/add_time",
                type: "post",
                dataType: "json",
                data: {"date": date, "scramble" : scramble, "time" : time},
                success: function(output){
                    cube_type = output['cube_type'];
                    session_list = output['session_list'];
                    history_list = output['history_list'];
                    session_statistics = output['session_statistics'];
                    history_statistics = output['history_statistics'];
                    fill_statistics("session", session_statistics);
                    fill_statistics("history", history_statistics);
                    fill_section("session", session_list);
                    fill_section("history", history_list);
                    generateHistogram(cube_type); 
                }
            });
            $("div#timer-container").removeClass("measure");
            clearInterval(timer);
            $("div#timer").html("00:00.00");
            unlock(["a.cube-type", "div#random-scramble"]);
            $("div#scramble").fadeTo(0, 0, randomScramble);
        }
        else if($("div#timer-container").hasClass("inspection")){
            lock(["a.cube-type", "div#random-scramble"]);
            var d = new Date();
            clearInterval(inspection);
            timer = setInterval(stopwatch_start, 2, d);
            $("div#timer-container").addClass("measure");
            $("div#timer-container").removeClass("inspection last-seconds");
        }
        else{
            lock(["a.cube-type", "div#random-scramble"]);
            var d = new Date();
            if($("input#insp")[0].checked){
                inspection = setInterval(stopwatch_inspection, 200, d);
                $("div#timer-container").addClass("inspection");
            }
            else{
                clearInterval(inspection);
                timer = setInterval(stopwatch_start, 2, d);
                $("div#timer-container").addClass("measure");
            }
        }
    }
}
$(document).ready(function(){

    default_operations();

    $("#section-headers div").on("click", "div", function(){
        var cls = $(this).attr("class");
        $("#section-headers div").removeAttr("id");
        $(this).attr("id", "active");
        $("div#section-content > div").hide();
        $("div#section-content > div."+cls).show();
    });
    $("#scramble").on("click", ".move", function(){
        animate_sequence(last = this);
    });
    $(document).on("click", ".cube-type", function(){
        change_type(new_cube_type = this);
    });
    $(document).on("click", "#random-scramble", function(){
        $("div#scramble").fadeTo(0, 0, randomScramble);    // Jeśli tylko timer nie jest włączony to losuje
    });
    $(document).on("click", "#excel", function(){
        $.ajax({
            url: "/export_to_excel",
            type: "post",
            success: function(){
                alert("Wyeksportowano do pliku Historia ułożeń.xlsx");
            }
        });
    });
    $(window).on("keydown", function(e){
        timerDown(e);
    });

    $(window).on("keyup", function(e){
        timerUp(e);
    });

    $(document).on("mousedown", "div#timer-container", function(){
        timerDown();
    });

    $(document).on("click", "div#timer-container", function(){
        timerUp();
    });
});

});


b4w.require("example_main").init();