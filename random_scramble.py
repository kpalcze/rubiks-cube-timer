import random

def set_moves(size):
	if size in [2,3]:
		return {"U", "D", "R", "L", "F", "B"}
	elif size in [4]:
		return {"U", "D", "R", "L", "F", "B", "Uw", "Dw", "Rw", "Lw", "Fw", "Bw"}
	else:
		raise Exception("Nie obsługujemy takiej kostki. Zły argument pzy losowaniu scramble'a.")

def random_scramble(size = 3):
	pairs = {}
	pairs["U"] = pairs["D"] = ("U", "D")
	pairs["F"] = pairs["B"] = ("F", "B")
	pairs["R"] = pairs["L"] = ("R", "L")
	pairs["Uw"] = pairs["Dw"] = ("Uw", "Dw")
	pairs["Fw"] = pairs["Bw"] = ("Fw", "Bw")
	pairs["Rw"] = pairs["Lw"] = ("Rw", "Lw")

	if size == 2: moves_no = 10
	elif size == 4: moves_no = 40
	else: moves_no = 25

	scramble = []
	moves =  set_moves(size)
	allowed = moves
	forbidden = set()
	types = ["'", "2", ""]

	for i in range(moves_no):
		move = random.choice(list(allowed))
		#forbidden.add(move)
		#forbidden.intersection_update(pairs[move])
		forbidden = set(pairs[move])
		allowed = moves - forbidden
		move+=random.choice(list(types))
		scramble.append(move)

	scramble = ["<div class = \"move\">" + move + "</div>" for move in scramble]
	scramble = " ".join(scramble)
	return scramble