% rebase('base.tpl', title='Python')
<div class="row">
	<div class="col-md-12" style="position:relative;">
		<div id="animation" style="position:relative">
			<div id="random-scramble" style="margin: 0 auto; width:200px; padding:15px; font-weight:bold; text-align: center; font-size: 13pt; word-spacing:5px;">Losuj scramble</div>
			<div id="scramble" style="margin: 0 auto; width:100%; padding:15px; font-weight:bold; text-align: center; font-size: 13pt; word-spacing:5px;">{{!randomScramble}}</div>
			<div id="canvas_cont" style="width:600px; height:400px; position:relative; margin:0 auto;"></div>
			<div id="timer-container" style="width:250px; border:1px solid #ccc; border-radius: 5px; font-size:14pt; margin:0 auto;text-align:center; margin-top:10px; padding:10px">Rozpocznij<div id="timer" style="clear:both">00:00.00</div></div>
		</div>
		<div id="section-headers">
			<div class="separate" style="width:33.33%; padding-right:5px"><div class="times">Sesja</div></div>
			<div class="separate" style="width:33.33%"><div class="statistics">Historia ułożeń</div></div>
			<div class="separate" style="width:33.33%; padding-left:5px"><div class="options">Opcje i histogram</div></div>
		</div>
		<div id="section-content">
			<div class="times">
				<div id="session-times"></div>
				<div class="separate" style="width:40%">
					<div id="session-data">
						<div class="header">Statystyki sesji</div>
						<div>Średnia z sesji: <span class="value"></span></div>
						<div>Najlepsza średnia z 3: <span class="value"></span></div>
						<div>Najlepsza średnia z 5: <span class="value"></span></div>
						<div>Najlepsza średnia z 12: <span class="value"></span></div>
						<div>Średnia z 3: <span class="value"></span></div>
						<div>Średnia z 5: <span class="value"></span></div>
						<div>Średnia z 12: <span class="value"></span></div>
						<div>Najlepsze ułożenie: <span class="value"></span></div>
						<div>Odchylenie standardowe: <span class="value"></span></div>
					</div>
				</div>
			</div>
			<div class="statistics">
				<div id="history-times"></div>
				<div class="separate" style="width:40%">
					<div id="history-data">
						<div class="header">Statystyki historii</div>
						<div>Średnia z historii: <span class="value"></span></div>
						<div>Najlepsza średnia z 3: <span class="value"></span></div>
						<div>Najlepsza średnia z 5: <span class="value"></span></div>
						<div>Najlepsza średnia z 12: <span class="value"></span></div>
						<div>Najlepsze ułożenie: <span class="value"></span></div>
						<div>Kwartyl 1/4: <span class="value"></span></div>
						<div>Kwartyl 3/4: <span class="value"></span></div>
						<div>Odchylenie standardowe: <span class="value"></span></div>
					</div>
				</div>
			</div>
			<div class="options">
				<div id="set-inspection">Czas na preinspekcje: &nbsp;<form style="display:inline"><input id="insp" type="checkbox" /></form></div>
				<div id="start-key">Klawisz stopera: <select id="start"><option>Space</option><option>Enter</option><option>0</option></select></div>
				<div id="histogram">
					<div class="header">Histogram</div>
					<div id="histogram-image"><img src="static/histogram{{cube_type}}.png" style="width:100%" /></div>
				</div>
			</div>
		</div>
	</div>
</div>