<!doctype html>
<html ng-app="MainApp">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <title>Puzzle Timer</title>
    <link rel="stylesheet" href="/static/css/bootstrap.css">
    <link rel="stylesheet" href="/static/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/static/css/main.css">

    <script src="/static/js/jquery.js"></script>
    <script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/static/js/b4w.min.js"></script>
    <script type="text/javascript" src="/static/js/animation.js"></script>
  </head>
  <body>

    <div class="container">
<!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="/">Puzzle Timer</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Typ kostki <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a class="cube-type" id="2" href="#">2x2x2</a></li>
                  <li><a class="cube-type" id="3" href="#">3x3x3</a></li>
                  <li><a class="cube-type" id="4" href="#">4x4x4</a></li> 
                </ul>
              </li>
              <li><a id="excel" href="#">Wyeksportuj do Excel</a></li>
            </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      {{!base}}
    </div>

  </body>
</html>
